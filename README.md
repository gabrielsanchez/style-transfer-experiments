# Style Transfer Experiments

If you've ever experimented with fast style transfer you know that it takes hours to train a model. I provide some of my models as ckpt folders if you want to use them and experiment yourself. I've tried artwork by Frida Kahlo, and dia de muertos art work. I'll be adding more as I do more experiments.

![alebrije](alebrije.png)

Here are some examples:

![diademuertos](diademuertos.jpg)

![frida](frida.jpg)

If you want to try them out just point to the folders when evaluating.

`python evaluate.py --checkpoint ckpt-frida --in-path img --ou
t-path out --allow-different-dimensions`

`python transform_video.py --in-path vid.mp4 --checkpoint ckpt
-frida --out-path out/vidfrida.mp4 --device /gpu:0 --batch-size 4`
